# Liquid Full-Stack Template
## A simple yet efficient way of deploying a full-stack website

This Template will be used to deploy a full-stack website containing Vue as the frontend and utilizing ExpressJS as the backend.

## Features

- Simplistic API setup
- Wonderful Vue frontend

This template is setup by [LovelessCodes](https://loveless.codes/) and utilized
in most of their work.

## Tech

This template uses a number of open source projects to work properly:

- [Vue] - JavaScript framework for web apps!
- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework [@tjholowaychuk]
- [jQuery] - duh

And of course this template itself is open source with a [public repository](https://gitlab.com/LovelessCodes/VueTemplate.git)
 on GitLab.

## Installation

This template requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

```sh
npm i
node app
```

For production environments...

```sh
npm install --production
NODE_ENV=production node app
```

## Development

Want to contribute? Great!

Open your favorite Terminal and run these commands.

First Tab:

```sh
node app
```
